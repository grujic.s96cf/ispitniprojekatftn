package com.ftninformatika.androidappdevzavrsnitest.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ftninformatika.androidappdevzavrsnitest.Activities.net.OMBDApiService;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.Movie;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.Search;
import com.ftninformatika.androidappdevzavrsnitest.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailMovie extends AppCompatActivity {

    TextView info;
    TextView title;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_movie);

        info = findViewById(R.id.text_detail);
        title = findViewById(R.id.title_detail);
        image = findViewById(R.id.image_detail);


        Search search = (Search) getIntent().getSerializableExtra("search");
        String movieId = search.getImdbID();

        callService(movieId);
    }

    private void callService(String query){
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("apikey", "339fcaf6");
        queryParams.put("i", query);

        Call<Movie> callMovie = OMBDApiService.apiInterface().searchMovie(queryParams);
        callMovie.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(retrofit2.Call<Movie> call, Response<Movie> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(DetailMovie.this, "Error loading", Toast.LENGTH_SHORT).show();

                }else{
                    Movie movie = response.body();
                    title.setText(movie.getTitle());
                    info.setText(movie.getPlot());
                    Picasso.get().load(movie.getPoster()).into(image);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<Movie> call, Throwable t) {
                Toast.makeText(DetailMovie.this,"Error "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }


}