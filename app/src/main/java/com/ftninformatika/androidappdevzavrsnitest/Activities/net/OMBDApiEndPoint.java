package com.ftninformatika.androidappdevzavrsnitest.Activities.net;

import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.Movie;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.SearchResult;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface OMBDApiEndPoint {

    @GET("/")
    Call<SearchResult> search(@QueryMap Map<String, String> options);


    @GET("/")
    Call<Movie> searchMovie(@QueryMap Map<String, String> options);


}
