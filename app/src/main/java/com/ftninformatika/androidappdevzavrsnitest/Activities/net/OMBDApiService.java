package com.ftninformatika.androidappdevzavrsnitest.Activities.net;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OMBDApiService {
    static OkHttpClient okHttpClient = new OkHttpClient();

    public static Retrofit getRetrofitInstance(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Contract.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


    public static OMBDApiEndPoint apiInterface(){
        OMBDApiEndPoint apiService = getRetrofitInstance().create(OMBDApiEndPoint.class);
        return apiService;
    }

}
