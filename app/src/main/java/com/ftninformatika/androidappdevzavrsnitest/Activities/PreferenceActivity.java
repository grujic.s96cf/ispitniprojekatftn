package com.ftninformatika.androidappdevzavrsnitest.Activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.ftninformatika.androidappdevzavrsnitest.R;

public class PreferenceActivity extends android.preference.PreferenceActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content,new PreferenceActivity.PrefFrag()).commit();

    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    public static class PrefFrag extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.settings);
        }
    }

}
