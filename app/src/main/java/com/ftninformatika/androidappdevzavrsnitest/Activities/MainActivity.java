package com.ftninformatika.androidappdevzavrsnitest.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ftninformatika.androidappdevzavrsnitest.Activities.Adapter.MojRecyclerViewAdapterListaFilmova;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.OMBDApiService;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.Search;
import com.ftninformatika.androidappdevzavrsnitest.Activities.net.model.SearchResult;
import com.ftninformatika.androidappdevzavrsnitest.R;

import java.io.Serializable;
import java.util.HashMap;


import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements Serializable,
        MojRecyclerViewAdapterListaFilmova.OnRecyclerItemClickListener{

    RecyclerView recViewLista;
Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        recViewLista = findViewById(R.id.rv_lista);

        final EditText query = findViewById(R.id.et_search);

        Button searchBtn = findViewById(R.id.btnSearch);


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callService(query.getText().toString().trim());
            }
        });

    }

    private void callService(String query){
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("apikey", "339fcaf6");
        queryParams.put("s", query);

        retrofit2.Call<SearchResult> call = OMBDApiService.apiInterface().search(queryParams);
        call.enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(retrofit2.Call<SearchResult> call, Response<SearchResult> response) {
                if (response.code() == 200){
                    SearchResult resp = response.body();
                    recViewLista.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    recViewLista.setAdapter(
                            new MojRecyclerViewAdapterListaFilmova(resp.getSearch(),MainActivity.this)
                    );
                }else{
                    Toast.makeText(MainActivity.this, "Error loading" + response.code(),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<SearchResult> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error "+t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }







    @Override
    public void onRVItemClick(Search search) {
        Intent intent = new Intent(MainActivity.this, DetailMovie.class);
        intent.putExtra("search",search);

        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()){
           case R.id.settings_main:
                   startActivity(new Intent(MainActivity.this,PreferenceActivity.class));
                   break;





       }
        return super.onOptionsItemSelected(item);

    }
    */
}

